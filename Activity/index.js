

// fetch("https://jsonplaceholder.typicode.com/posts/").then((response) => response.json()
// ).then((json) => console.log(json))



fetch("https://jsonplaceholder.typicode.com/todos").then((response => response.json())).then(data => (data.map((value) => { return value.title }))).then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1").then((response) => response.json()
).then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos", {

    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
        completed: false,
        id: 201,
        title: 'Created To Do List Item',
        userID: 1
    })

}).then(response => response.json()).then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {

    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        id: 1,
        title: 'Updated To Do List Item',
        userID: 1
    })

}).then(response => response.json()).then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {

    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
        completed: false,
        dateCompleted: "07/09/21",
        status: "Complete"
    })

}).then(response => response.json()).then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
})
